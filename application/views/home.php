<?php $this->load->view('header') ?>
    <div class="container">
        <div class="row justify-content-center mt-5">
            <div class="col-md-6">
                <h2>Selamat Datang di Home </h2>
                <p>Anda masuk sebagai <?= $user; ?></p>
                <a href="<?= base_url('login/logout') ?>" class="btn btn-danger">Logout</a>
            </div>
        </div>
    </div>
<?php $this->load->view('footer') ?>
