<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {


	function __construct()
	{
		parent::__construct();
		$this->load->model('auth_model');
		
	}

	public function index()
	{
		if ($this->session->userdata('role')=='admin') {
			redirect('/home');
		}elseif($this->session->userdata('role')=='karyawan'){
			redirect('/karyawan');
		}

		$this->load->view('login');

	}
	public function autentikasi(){
		if ($this->session->userdata('role')=='admin') {
			redirect('/home');
		}elseif($this->session->userdata('role')=='karyawan'){
			redirect('/karyawan');
		}

		$username = $this->input->post('username');
        $password = $this->input->post('password');
        $user = $this->auth_model->get_user($username, $password);

        if ($user) {
        	
        	$userdata = array(
                    'user_id' => $user->id,
                    'role' => $user->role,
                );


        	if ($user->role=='admin') {
        		$this->session->set_userdata($userdata);
           		 redirect('/home');
        	}elseif($user->role=='karyawan'){
        		$this->session->set_userdata($userdata);
           		 redirect('/karyawan');
        	}
            
        }else{
        	$this->session->set_flashdata('gagal', 'User atau Password anda salah');
			redirect('/login');
        }
    }

    public function logout(){
    	$this->session->sess_destroy();
    	$url=base_url('login');
    	redirect($url);
    }
}
