<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('role')!='admin') {
			redirect('/login');
		}
	}
	public function index()
	{
		$data['user'] = $this->session->userdata('role');
		$this->load->view('home',$data);
	}
}
